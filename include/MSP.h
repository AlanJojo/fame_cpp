#pragma once

#include <map>
#include "support.h"

class MSP {
  public:
    int len_longest_row;

    MSP() {
      len_longest_row = 1;
    }

    BinNode* createPolicy(string policy_string) {
    	PolicyParser parser = PolicyParser();
    	BinNode *policy_obj = parser.parse(policy_string);
    	map<string,int> dictCount, dictLabel;
    	parser.findDuplicates(policy_obj, dictCount);
    	for(auto i:dictCount) {
        if(i.second>1) {
          dictLabel[i.first] = 0;
				}
			}
      parser.labelDuplicates(policy_obj, dictLabel);
      return policy_obj;
    }

    map<string, vector<int>> convertPolicyToMSP(BinNode *tree) {
      vector<int> root_vector(1,1);
      len_longest_row = 1;
      return convertPolicyToMSP_Helper(tree, root_vector);
    }

  protected:
    map<string, vector<int>> convertPolicyToMSP_Helper(BinNode *subtree, vector<int> &curr_vector) {
      map<string, vector<int>> um;
      if(!subtree) {
        return um;
			}
            
      string type = subtree->getNodeType();
      string attr = "ATTR";
      string or_node = "OR";
      string and_node = "AND";
      if(type == attr) {
        um[subtree->getAttributeAndIndex()] = curr_vector;
        return um;
      }
      else if(type == or_node) {
        map<string, vector<int>> left_dict, right_dict;
        left_dict = convertPolicyToMSP_Helper(subtree->getLeft(), curr_vector);
        right_dict = convertPolicyToMSP_Helper(subtree->getRight(), curr_vector);
        left_dict.insert(right_dict.begin(), right_dict.end());
        return left_dict;
      }
      else if(type == and_node) {
        int length = curr_vector.size();
        vector<int> left_vector = curr_vector;
        for(int i=0; i<len_longest_row-length; i++) {
          left_vector.push_back(0);
				}
        left_vector.push_back(1);
        vector<int> right_vector(len_longest_row,0);
        right_vector.push_back(-1);
        len_longest_row++;
        map<string, vector<int>> left_dict, right_dict;
        left_dict = convertPolicyToMSP_Helper(subtree->getLeft(), left_vector);
        right_dict = convertPolicyToMSP_Helper(subtree->getRight(), right_vector);
        left_dict.insert(right_dict.begin(), right_dict.end());
        return left_dict;
      }
      return um;
    }
};