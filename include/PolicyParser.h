#pragma once

#include <vector>
#include "support.h"

class PolicyParser{
  public:
    bool verbose;
    PolicyParser(bool v = false) {
      verbose = v;        
    }

    vector<string> get_tokens(string line) {
      char ch;
      int i= 0, level= 0;
      string attr;
      vector<string> tokens;
      do {
        ch = line[i];
        if(ch=='(')
          tokens.push_back("(");
        else if(ch==')')
          tokens.push_back(")");
        else if((ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || (ch>='0' && ch<='9') || ch=='!') {
          if((ch=='a' || ch=='A') && (line[i+1]=='n' || line[i+1]=='N') && (line[i+2]=='d' || line[i+2]=='D')) {
            tokens.push_back("AND");
            i+=2;
            level++;
          }
          else if((ch=='o' || ch=='O') && (line[i+1]=='r' || line[i+1]=='R')) {
            tokens.push_back("OR");
            i+=1;
            level++;
          }
          else {
            attr = ch;
            i++;
            bool parenEnd = false;
            while(((ch = line[i])!=')') && ch!=' ' && ch!= '\n' ) {
              attr.append(1,ch);
              i++;
              if(line[i]==')') {
                parenEnd = true;
              }
            }
            tokens.push_back(attr);
            if(parenEnd)
              tokens.push_back(")");
          }
        }
        else if(ch==' ');
        i++;
      } while(line[i]);
      return tokens;
    }

    BinNode* S(unsigned int* index, vector<string> tokens) {
      BinNode *left = T(index, tokens);
      if(*index < tokens.size() && tokens[*index]=="OR") {
        (*index)++;
        BinNode *right = S(index, tokens);
        return new BinNode("OR", left, right);
      }
      return left;
    }

    BinNode* T(unsigned int* index, vector<string> tokens) {
      BinNode *left = F(index, tokens);
      if(*index < tokens.size() && tokens[*index]=="AND") {
        (*index)++;
        BinNode *right = T(index, tokens);
        return new BinNode("AND", left, right);
      }
      return left;
    }

    BinNode* F(unsigned int* index, vector<string> tokens) {
      string token = tokens[*index];
      (*index)++;
      BinNode *node;
      if(token=="(") {
        node = S(index, tokens);
        (*index)++;
      }
      else
        node = new BinNode(token);
      return node;
    }

    BinNode* parse(string line) {
      /*
          S → T '+' S | T
          T → F '.' T | F
          F → A | '('S')'
          A → 'a' | 'b' | 'c' | ... | ɛ
      */
      vector<string> tokens = get_tokens(line);
      unsigned int index = 0;
      return S(&index, tokens);
    }

    void findDuplicates(BinNode* tree, map<string,int> &dict) {
      if(tree->getLeft())
        findDuplicates(tree->getLeft(), dict);
      if(tree->getRight())
        findDuplicates(tree->getRight(), dict);
      string attr = "ATTR";
      if(tree->getNodeType() == attr) {
        string key = tree->getAttribute();
        dict[key] = dict.find(key) == dict.end() ? 1 : dict[key]+1;
      }
    }
        
    void labelDuplicates(BinNode* tree, map<string,int> &dictLabel) {
      if(tree->getLeft())
        labelDuplicates(tree->getLeft(), dictLabel);
      if(tree->getRight())
        labelDuplicates(tree->getRight(), dictLabel);
      string attr = "ATTR";
      if(tree->getNodeType() == attr) {
        string key = tree->getAttribute();
        if(dictLabel.find(key) != dictLabel.end()) {
          tree->setIndex(dictLabel[key]);
          dictLabel[key]++;
        }
      }
    }

    pair<bool, vector<BinNode*>> requiredAttributes(BinNode *tree, vector<string> &attrList) {
      vector<BinNode*> sendThis;
      if(!tree)
        return make_pair(false, sendThis);
      BinNode *left = tree->getLeft();
      BinNode *right = tree-> getRight();
      pair<bool, vector<BinNode*>> return_pair_left, return_pair_right;

      if(left)
        return_pair_left = requiredAttributes(left, attrList);
      if(right)
        return_pair_right = requiredAttributes(right, attrList);
      string type = tree->getNodeType();
      string attr = "ATTR";
      string or_node = "OR";
      string and_node = "AND";

      if(type == or_node) {
        if(return_pair_left.first)
          sendThis = return_pair_left.second;
        else if(return_pair_right.first)
          sendThis = return_pair_right.second;
        return make_pair(return_pair_left.first||return_pair_right.first, sendThis);  
      }
      else if(type == and_node) {
        if(return_pair_left.first&&return_pair_right.first) {
          sendThis.reserve(return_pair_left.second.size() + return_pair_right.second.size());
          sendThis.insert(sendThis.end(), return_pair_left.second.begin(), return_pair_left.second.end());
          sendThis.insert(sendThis.end(), return_pair_right.second.begin(), return_pair_right.second.end());
        }
        else if(return_pair_left.first) {
          sendThis.reserve(return_pair_left.second.size());
          sendThis.insert(sendThis.end(), return_pair_left.second.begin(), return_pair_left.second.end());
        }
        else if(return_pair_right.first) {
          sendThis.reserve(return_pair_right.second.size());
          sendThis.insert(sendThis.end(), return_pair_right.second.begin(), return_pair_right.second.end());
        }
        return make_pair(return_pair_left.first&&return_pair_right.first, sendThis);
      } 
      else if(type == attr) {
        if(find(attrList.begin(), attrList.end(), tree->getAttribute())!=attrList.end()) {
          sendThis.push_back(tree);
          return make_pair(true, sendThis);  
        }
        else
          return make_pair(false, sendThis);
      }
      return make_pair(false, sendThis);
    }

    vector<BinNode*> prune(BinNode *tree, vector<string> attributes) {
      pair<bool, vector<BinNode*>> return_pair = requiredAttributes(tree, attributes);
      vector<BinNode*> emptyPrunedList;
      if(!return_pair.first)
        return emptyPrunedList;
      return return_pair.second;
    }
};