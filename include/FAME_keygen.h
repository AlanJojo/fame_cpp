#pragma once

#include <array>
#include "support.h"

#ifdef DEBUG_KEYGEN
	#define debk(x,y) \
    cout << x; \
    element_printf("%B\n", y)
#else
	#define debk(x,y)
#endif

void Keygen(map<string, array<element_t, 3>> &sk, element_t *master_secret_key, vector<string> attribute_list, pairing_t pairing) {
  #ifdef DEBUG_KEYGEN
    cout << "\n-------------------------------- Get into Keygen()\n";
  #endif

  element_t r[2], r_sum, br[2], temp_G, temp_Z, term[4], pow[4][2], sigma_y, sigma_;
  array<element_t, 3> node;

  element_init_Zr(r_sum, pairing);
  element_set0(r_sum);
  for(int i = 0; i < 2; i++) {
    element_init_Zr(r[i], pairing);
    element_random(r[i]);       // r1, r2
    element_add(r_sum, r_sum, r[i]);
  }
  debk("\nr1 = ", r[0]);
  debk("r2 = ", r[1]);
  debk("r1+r2 = ", r_sum);

  // Compute br for later use
  for(int i = 0; i < 2; i++) {
    element_init_Zr(br[i], pairing);
    element_mul(br[i], master_secret_key[4+i], r[i]);   // b1*r1, b2*r2
  }
  debk("b1*r1 = ", br[0]);
  debk("b2*r2 = ", br[1]);

  // Compute sk0
  for(int i = 0; i < 2; i++) {
    element_init_G2(node[i], pairing);
    element_pow_zn(node[i], master_secret_key[1], br[i]);    // h^(b1r1), h^(b2r2)
  }
  element_init_G2(node[2], pairing);
  element_pow_zn(node[2], master_secret_key[1], r_sum);    // h^(r1+r2)
  debk("\nh^(b1*r1) = ", node[0]);
  debk("h^(b2*r2) = ", node[1]);
  debk("h^(r1+r2) = ", node[2]);

  sk["SK0"] = node;

  // Pre-compute powers
  for(int t = 0; t < 2; t++) {
    for(int i = 0; i < 2; i++) {
      element_init_Zr(pow[i][t], pairing);
      element_div(pow[i][t], br[i], master_secret_key[2+t]);    // (b1r1)/a1, (b1r1)/a2, (b2r2)/a1, (b2r2)/a2
    }
    element_init_Zr(pow[2][t], pairing);
    element_div(pow[2][t], r_sum, master_secret_key[2+t]);    // (r1+r2)/a1, (r1+r2)/a2
    element_init_Zr(pow[3][t], pairing);
  }
  debk("\nb1*r1 / a1 = ", pow[0][0]);
  debk("b1*r1 / a2 = ", pow[0][1]);
  debk("b2*r2 / a1 = ", pow[1][0]);
  debk("b2*r2 / a2 = ", pow[1][1]);
  debk("(r1+r2) / a1 = ", pow[2][0]);
  debk("(r1+r2) / a2 = ", pow[2][1]);

  // sk_y,t
  for (int i = 0; i < 4; i++) {
    element_init_G1(term[i], pairing);
  }
  element_init_Zr(sigma_y, pairing);
  element_init_Zr(temp_Z, pairing);
  element_init_G1(temp_G, pairing);

  // For all y in attribute_list
  for (auto y:attribute_list) {
    element_random(sigma_y);
    debk("\nsigma_y = ", sigma_y);

    // For t = 0, 1
    for (int t = 0; t<2; t++) {
      element_init_G1(node[t], pairing);

      hash_to_G(term[0], 0, y, 0, t, pairing);  // H(y0t)
      hash_to_G(term[1], 0, y, 1, t, pairing);  // H(y1t)
      hash_to_G(term[2], 0, y, 2, t, pairing);  // H(y2t)

      // [H(y0t) ^ ((b1 * r1) / at)] . [H(y1t) ^ ((b2 * r2) / at)] . [H(y2t) ^ ((r1 + r2) / at)]
      element_pow3_zn(temp_G, term[0], pow[0][t], term[1], pow[1][t], term[2], pow[2][t]);

      element_div(pow[3][t], sigma_y, master_secret_key[2+t]);  // sigma_y / at
      debk("\n  sigma_y / at = ", pow[3][t]);

      element_pow_zn(term[3], master_secret_key[0], pow[3][t]); // g ^ (sigma_y / at)

      element_mul(node[t], temp_G, term[3]);    // sk_y_t

    	#ifdef DEBUG_KEYGEN
    	  cout << "  H(" << y;
    	  element_printf("0%d) = %B", t, term[0]);
    	  cout << "\n  H(" << y;
    	  element_printf("1%d) = %B", t, term[1]);
    	  cout << "\n  H(" << y;
    	  element_printf("2%d) = %B", t, term[2]);
    	  debk("\n  term[0].term[1].term[2] = ", temp_G);
    	  debk("  g ^ (sigma_y / at) = ", term[3]);
    	  cout << "  sk(" << y;
    	  element_printf(", %d) = %B\n", t, node[t]);
    	#endif
    }

  	element_init_G1(node[2], pairing);
  	element_neg(temp_Z, sigma_y);    // -sigma_y
  	debk("\n  - sigma_y = ", temp_Z);
  	element_pow_zn(node[2], master_secret_key[0], temp_Z);    // g ^ -sigma_y
  	debk("  g ^ -sigma_y = ", node[2]);

    sk[y] = node;
  }
  element_clear(sigma_y);

  // sk'
  element_init_Zr(sigma_, pairing);
  element_random(sigma_);
  debk("\nsigma_ = ", sigma_);

  // For t = 1, 2
  for (int t = 0; t<2; t++) {
  	element_init_G1(node[t], pairing);
    hash_to_G(term[0], 1, "", 0, t, pairing);  // H(010t)
    hash_to_G(term[1], 1, "", 1, t, pairing);  // H(011t)
    hash_to_G(term[2], 1, "", 2, t, pairing);  // H(012t)
            
    // [H(010t) ^ ((b1 * r1) / at)] . [H(011t) ^ ((b2 * r2) / at)] . [H(012t) ^ ((r1 + r2) / at)]
    element_pow3_zn(temp_G, term[0], pow[0][t], term[1], pow[1][t], term[2], pow[2][t]);

    element_mul(node[t], master_secret_key[6+t], temp_G);

    element_div(pow[3][t], sigma_, master_secret_key[2+t]);  // sigma_ / at
    debk("\n  sigma_ / at = ", pow[3][t]);

    element_pow_zn(term[3], master_secret_key[0], pow[3][t]); // g ^ (sigma_ / at)
    element_mul(node[t], node[t], term[3]);    // sk'_t

    #ifdef DEBUG_KEYGEN
      element_printf("  H(010%d) = %B", t, term[0]);
      element_printf("\n  H(011%d) = %B", t, term[1]);
      element_printf("\n  H(012%d) = %B", t, term[2]);
      debk("\n  term[0].term[1].term[2] = ", temp_G);
      debk("  g ^ (sigma_ / at) = ", term[3]);
      element_printf("  sk'_%d = %B\n", t, node[t]);
    #endif
  }

  element_init_G1(node[2], pairing);
  element_neg(temp_Z, sigma_);     // -sigma_
  debk("\n  -sigma_ = ", temp_Z);
  element_pow_zn(temp_G, master_secret_key[0], temp_Z);     // g^(-sigma_)
  debk("  g^(-sigma_) = ", temp_G);
  element_mul(node[2], master_secret_key[8], temp_G);   // (g^d3) * (g^(-sigma_))
  debk("  (g^d3) * (g^(-sigma_)) = ", node[2]);

  sk["SK_dash"] = node;

  for (int i = 0; i < 4; i++) {
    element_clear(term[i]);
    for (int j = 0; j < 2; j++)
      element_clear(pow[i][j]);
  }
  for (int i = 0; i < 2; i++) {
    element_clear(r[i]);
    element_clear(br[i]);
  }
  element_clear(r_sum);
  element_clear(sigma_);
  element_clear(temp_Z);
  element_clear(temp_G);

  #ifdef DEBUG_KEYGEN
    cout << "\n-------------------------------- Exit Keygen()\n";
  #endif
}