#pragma once

#include "support.h"

#ifdef DEBUG_DECRYPT
	#define debd(x,y) \
  	cout << x; \
  	element_printf("%B\n", y)
#else
	#define debd(x,y)
#endif

void Decrypt(element_t decrypted_message, vector<string> attribute_list, element_t* public_key, BinNode* policy, map<string, array<element_t, 3>> cipher_text, map<string, array<element_t, 3>> sk, pairing_t pairing) {
  #ifdef DEBUG_DECRYPT
    cout << "\n-------------------------------- Get into Decrypt()\n";
  #endif

  vector<BinNode *> nodes;
  PolicyParser parser;
  nodes = parser.prune(policy, attribute_list);
  if(nodes.empty()) {
    cout << "\nPolicy not satisfied\n";
    return;
  }
  #ifdef DEBUG_DECRYPT
    cout << "\nnodes\n";
    for (auto i:nodes)
      walkThrough(i, "", true);
  #endif

  element_t term[4], num, den;
  string attr, attr_stripped;

  element_init_GT(num, pairing);
  element_init_GT(den, pairing);
  for (int i = 0; i < 2; i++) {
    element_init_G1(term[i], pairing);
    element_init_GT(term[2+i], pairing);
  }

  element_set1(num);
  element_set1(den);

  for (int i = 0; i < 3; i++) {
    #ifdef DEBUG_DECRYPT
      element_printf("\n(i=%d)\n", i);
    #endif

    element_set1(term[0]);
    element_set1(term[1]);

    for(auto node:nodes) {
      attr = node->getAttributeAndIndex();
      #ifdef DEBUG_DECRYPT
        cout << "  " << attr << endl;
      #endif

      attr_stripped = strip_index(attr);
      element_mul(term[0], term[0], sk[attr_stripped][i]);
      #ifdef DEBUG_DECRYPT
        cout << "    term[0] *= sk[" << attr_stripped;
        element_printf("][%d] = %B\n", i, term[0]);
      #endif
      element_mul(term[1], term[1], cipher_text[attr][i]);
      #ifdef DEBUG_DECRYPT
        cout << "    term[1] *= cipher_text[" << attr;
        element_printf("][%d] = %B\n", i, term[1]);
      #endif
    }

    element_mul(term[0], term[0], sk["SK_dash"][i]);
    element_pairing(term[2], term[0], cipher_text["CT0"][i]);
    #ifdef DEBUG_DECRYPT
      element_printf("  term[2] = %B\n", term[2]);
    #endif

    element_mul(den, den, term[2]);
    #ifdef DEBUG_DECRYPT
      element_printf("  den = %B\n", den);
    #endif

    element_pairing(term[3], term[1], sk["SK0"][i]);
    #ifdef DEBUG_DECRYPT
      element_printf("  term[3] = %B\n", term[3]);
    #endif

    element_mul(num, num, term[3]);
    #ifdef DEBUG_DECRYPT
      element_printf("  num = %B\n", num);
    #endif
  }

  element_mul(num, num, cipher_text["CT_DASH"][2]);               // num set
  #ifdef DEBUG_DECRYPT
    element_printf("\nnum = %B\n", num);
  #endif

  element_div(decrypted_message, num ,den);
  #ifdef DEBUG_DECRYPT
    element_printf("\ndecrypted_message = %B\n", decrypted_message);
  #endif

  element_clear(num);
  element_clear(den);
  for (int i = 0; i < 4; i++) {
    element_clear(term[i]);
	}

  #ifdef DEBUG_DECRYPT
    cout << "\n-------------------------------- Exit Decrypt()\n";
  #endif

}