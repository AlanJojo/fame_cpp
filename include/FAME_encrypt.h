#pragma once

#include "support.h"

#ifdef DEBUG_ENCRYPT
	#define debe(x,y) \
    cout << x; \
    element_printf("%B\n", y)
#else
	#define debe(x,y)
#endif

BinNode* Encrypt(map<string, array<element_t, 3>>& cipher_text, element_t* public_key, string policy_string, element_t message, pairing_t pairing) {
  #ifdef DEBUG_ENCRYPT
    cout << "\n-------------------------------- Get into Encrypt()\n";
  #endif

  MSP msp = MSP();
  BinNode *policy = msp.createPolicy(policy_string);
  #ifdef DEBUG_ENCRYPT
    cout << "\nPolicy tree\n";
    walkThrough(policy, "", true);
  #endif
  map<string, vector<int>> M = msp.convertPolicyToMSP(policy);
  #ifdef DEBUG_ENCRYPT
    cout << "\nMSP = \n";
    for(auto i:M) {
      cout << i.first << "\t";
      for(auto j:i.second)
        cout << j << " ";
      cout << endl;
    }
    cout << "\nMSP.len_longest_row = " << msp.len_longest_row << endl;
  #endif

  element_t s[2], temp_Z, temp_G, term[2];

  element_init_Zr(temp_Z, pairing);
  for(int i=0; i< 2; i++) {
    element_init_Zr(s[i], pairing);
    element_random(s[i]);
    element_add(temp_Z, temp_Z, s[i]);     // s1+s2
  }
  debe("\ns1 = ", s[0]);
  debe("s2 = ", s[1]);
  debe("s1 + s2 = ", temp_Z);

  array<element_t, 3> node;

  // ct0
  for(int i = 0; i < 2; i++) {
    element_init_G2(node[i], pairing);
    element_pow_zn(node[i], public_key[1+i], s[i]);  // H1^s1, H2^s2
  }
  debe("\nH1 ^ s1 = ", node[0]);
  debe("H2 ^ s2 = ", node[1]);

  element_init_G2(node[2], pairing);
  element_pow_zn(node[2], public_key[0], temp_Z);   // h^(s1+s2)
  debe("h ^ (s1 + s2) = ", node[2]);

  cipher_text["CT0"] = node;

  int count, n2 = msp.len_longest_row;   // MSP has n1 rows and n2 columns
  element_t hash_table[n2][3][2];

  // Pre-compute hashes
  for (int j = 0; j < n2; j++) {
    for (int l = 0; l < 3; l++) {
      for (int t = 0; t < 2; t++) {
        element_init_G1(hash_table[j][l][t], pairing);
        hash_to_G(hash_table[j][l][t], j+1, "", l, t, pairing);
        #ifdef DEBUG_ENCRYPT
          cout << "\nH(0" << j+1 << l << t << ")";
          element_printf(" = %B", hash_table[j][l][t]);
        #endif
      }
    }
  }

  string str = "";
  // ct_i,l
  for (int i = 0; i < 2; i++) {
    element_init_G1(term[i], pairing);
	}
  element_init_G1(temp_G, pairing);

  for(auto row:M) {
    str = strip_index(row.first);
    for(int l = 0; l < 3; l++) {
      element_init_G1(node[l], pairing);
      for(int t = 0; t < 2; t++) {
        hash_to_G(term[t], 0, str, l, t, pairing);
        #ifdef DEBUG_ENCRYPT
          cout << "\n\nH(" << str;
          element_printf("%d%d) = %B\n", l, t, term[t]);
        #endif

        count = 0;
      	for (auto j:row.second) {
      	  if(!j) {       // Don't multiply with zero. Never do this.
      	    count++;
      	    continue;
      	  }
      	  else if(j==1)
      	    element_set(temp_G, hash_table[count][l][t]);       // H(0jlt) ^ M[i][j]
      	  else if(j==-1)
      	    element_invert(temp_G, hash_table[count][l][t]);       // H(0jlt) ^ M[i][j]
      	  #ifdef DEBUG_ENCRYPT
      	    cout << "    H(0" << count+1 << l << t << ") ^ "  << j << " = ";
      	    element_printf("%B\n", temp_G);
      	  #endif

          element_mul(term[t], term[t], temp_G);
          #ifdef DEBUG_ENCRYPT
            cout << "    (j=" << j << ") Hs" << t;
            element_printf(" = %B\n", term[t]);
          #endif

          count++;
        }

        element_pow_zn(term[t], term[t], s[t]);
        #ifdef DEBUG_ENCRYPT
          cout << "  pow Hs" << t;
          element_printf(" = %B", term[t]);
        #endif
      }

      element_mul(node[l], term[0], term[1]);
      #ifdef DEBUG_ENCRYPT
        cout << "\nct_" << row.first << "," << l;
        element_printf(" = %B", node[l]);
      #endif
    }
    cipher_text[row.first] = node;   // ct_i
  }

  element_clear(temp_G);
  for (int t = 0; t < 2; t++) {
    element_clear(term[t]);
    for (int j = 0; j < n2; j++)
      for (int l = 0; l < 3; l++)
        element_clear(hash_table[j][l][t]);
  }
  element_clear(temp_Z);

  // ct'
  element_init_GT(node[0], pairing);
  element_set1(node[0]);
  element_init_GT(node[1], pairing);
  element_set1(node[1]);
  element_init_GT(node[2], pairing);
  element_pow2_zn(node[2], public_key[3], s[0], public_key[4], s[1]);   // T1^s1 * T2^s2
  debe("\n\nT1^s1 * T2^s2 = ", node[2]);
  element_mul(node[2], node[2], message);    // T1^s1 * T2^s2 * message
  debe("\nct' = ", node[2]);

  cipher_text["CT_DASH"] = node;

  element_clear(s[0]);
  element_clear(s[1]);

  #ifdef DEBUG_ENCRYPT
    cout << "\n-------------------------------- Exit Encrypt()\n";
  #endif

  return policy;
}