#pragma once

#define OPENSSL_API_COMBAT 0x10100000L

#include <cstring>
#include <math.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <pbc/pbc.h>
#include "BinaryNode.h"
#include "PolicyParser.h"
#include "MSP.h"

int hash_to_bytes(uint8_t *input_buf, int input_len, uint8_t *output_buf, int hash_len) {
  #ifdef DEBUG_SUPPORT
    printf("\n\n-------------------------------- Enter hash_to_bytes()\n");
    printf("input_buf = %s\n", input_buf);
    printf("input_len = %d\n", input_len);
    printf("hash_len = %d\n", hash_len);
  #endif

  const int new_input_len = input_len + 2; // extra byte for prefix
  uint8_t new_input[new_input_len];
  int HASH_256_LEN;

  // OpenSSL 3.0 implementation
  EVP_MD_CTX *ctx =NULL;
  EVP_MD *sha256 = NULL;
  unsigned int len = 0;
  unsigned char *outdigest = NULL;
  int ret = 1;
  int blocks;

  memset(new_input, 0, new_input_len);
  new_input[0] = (uint8_t)1; // block number (always 1 by default)
  #ifdef DEBUG_SUPPORT
    printf("\nnew_input[0] = %02x\n", new_input[0]);
  #endif
  new_input[1] = (uint8_t)2; // set hash prefix
  #ifdef DEBUG_SUPPORT
    printf("new_input[1] = %02x\n", new_input[1]);
  #endif
  memcpy(new_input+2, input_buf, input_len); // copy input bytes

  // prepare output buf
  memset(output_buf, 0, hash_len);

  /* Create a context for the digest operation */
  ctx = EVP_MD_CTX_new();
  if (ctx == NULL) {
    goto err;
  }

  /*
  * Fetch the SHA256 algorithm implementation for doing the digest. We're
  * using the "default" library context here (first NULL parameter), and
  * we're not supplying any particular search criteria for our SHA256
  * implementation (second NULL parameter). Any SHA256 implementation will
  * do.
  */
  sha256 = EVP_MD_fetch(NULL, "SHA256", NULL);
  if (sha256 == NULL) {
    goto err;
  }

  HASH_256_LEN = EVP_MD_get_size(sha256);

  /* Allocate the output buffer 
   * Apply variable-size hash technique to get desired size determine block count. */
  blocks = (int) ceil(((double) hash_len) / HASH_256_LEN);
  outdigest = (unsigned char *) OPENSSL_malloc(HASH_256_LEN * blocks);
  if (outdigest == NULL) {
    goto err;
  }

  if (hash_len <= HASH_256_LEN) {
    // Initialise the digest operation 
    if (!EVP_DigestInit_ex(ctx, sha256, NULL)) {
      goto err;
    }

    // Pass the message to be digested
    if (!EVP_DigestUpdate(ctx, new_input, new_input_len)) {
      goto err;
    }

    // Now calculate the digest itself 
    if (!EVP_DigestFinal_ex(ctx, outdigest, &len)) {
      goto err;
    }
  }
  else {
    for(int i = 0; i < blocks; i++) {
      /* Initialise the digest operation */
      if (!EVP_DigestInit_ex(ctx, sha256, NULL)) {
        goto err;
      }

      // compute digest = SHA-2( i || prefix || input_buf ) || ... || SHA-2( n-1 || prefix || input_buf )
      new_input[0] = (uint8_t)(i+1);
      
      // Pass the message to be digested.
      if (!EVP_DigestUpdate(ctx, new_input, new_input_len)) {
        goto err;
      }

      /* Now calculate the digest itself */
      if (!EVP_DigestFinal_ex(ctx, outdigest + (i * HASH_256_LEN), &len)) {
        goto err;
      }
    }
  }

  memcpy(output_buf, outdigest, hash_len);
  #ifdef DEBUG_SUPPORT
    printf("output_buf = ");
    for(int i=0; i<hash_len; i++) {
      printf("%02x ", output_buf[i]);
    }
    printf("\n");
  #endif

  ret = 0;

  err:
  /* Clean up all the resources we allocated */
  OPENSSL_free(outdigest);
  EVP_MD_free(sha256);
  EVP_MD_CTX_free(ctx);
  if (ret != 0) {
    ERR_print_errors_fp(stderr);
  }

  #ifdef DEBUG_SUPPORT
    printf("-------------------------------- Exit hash_to_bytes()\n");
  #endif
  return ret;
}

void hash_to_G(element_t output, int j, string x, int l, int t, pairing_t pairing) {
  /*
   * If j is 0, it means input is of form (x,l,t) represented as "xlt" where x is string
   * else input is of form (j,l,t) represented as "0jlt" where j is a positive integer */
  char *cstr = x.data(), *str = x.data();
  if(j==0) {
    std::sprintf(str, "%s%d%d", cstr, l, t);
  }
  else {
    std::sprintf(str, "0%d%d%d", j, l, t);
  }

  int hash_len = mpz_sizeinbase(pairing->r, 2) / 8;
  uint8_t hash_buf[hash_len];
  memset(hash_buf, 0, hash_len);
  int result = hash_to_bytes((uint8_t *) str, (int) strlen(str), hash_buf, hash_len);

  if(!result) {
    element_from_hash(output, hash_buf, hash_len);
  }
}

string strip_index(string node_str) {
  size_t pos = node_str.find('_');
  if(pos !=string::npos) {
    return node_str.substr(0,pos);
  }
  return node_str;
}

void walkThrough(BinNode *root, string indent, bool last) {
  if(root) {
    if(root->getNodeType()=="AND"||root->getNodeType()=="OR")
      cout<<indent<<"+- "<<root->getNodeType()<<endl;
    else
      cout<<indent<<"+- "<<root->getNodeType()<<"("<<root->getAttributeAndIndex()<<")"<<endl;
  }
  indent += last ? "   " : "|  ";
  if(root->getLeft() && root->getRight()) {
    walkThrough(root->getLeft(), indent, false);
    walkThrough(root->getRight(), indent, true);
  }
  else if(root->getLeft())
    walkThrough(root->getLeft(), indent, true);
}