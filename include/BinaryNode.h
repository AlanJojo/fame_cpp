#pragma once

#include <algorithm>
#include "support.h"

class BinNode {
  private:
    int index;
    bool negated;
    string node_type;
    string attribute;
    BinNode* left;
    BinNode* right;
  public:
    BinNode(string value, BinNode *leftn = NULL, BinNode *rightn = NULL) {
      if(value=="and"||value=="AND")
        node_type = "AND";
      else if(value=="or"||value=="OR")
        node_type = "OR";
      else {
        negated = false;
        index = -1; //None
        if(value[0]=='!') {
          value = value.substr(1, value.size());
          negated = true;
        }
        size_t pos = value.find('_');
        if(pos !=string::npos) {
          string rest = value.substr(pos+1, value.size());
          value = value.substr(0, pos);
          pos = rest.find('_');
          index = pos != string::npos ? stoi(rest.substr(0,pos)) : stoi(rest);
        }

        node_type = "ATTR";
        transform(value.begin(), value.end(), value.begin(), ::toupper);        
        attribute = value;
      }

      left = leftn;
      right = rightn;
    }

    string getAttribute() {
      string return_val = "";
      string attr = "ATTR";
      if(node_type==attr) {
        if(negated)
          return_val += '!';
        return_val += attribute;
      }
      return return_val;
    }

    string getAttributeAndIndex() {
      string return_val = "";
      string attr = "ATTR";
      if(node_type==attr) {
        if(negated)
          return_val += '!';
        return_val += attribute;
        if(index != -1)
          return_val += '_'+to_string(index);
      }
        return return_val;
    }

    string getNodeType() {
      return node_type;
    }

    BinNode* getLeft() {
      return left;
    }

    BinNode* getRight() {
      return right;
    }

    void setIndex(int value) {
      index = value;
    }

    void clean() {
      if(left) {
        left->clean();
        delete left;
      }
      if(right) {
        right->clean();
        delete right;
      }
    }
};