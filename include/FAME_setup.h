#pragma once

#include "support.h"

#ifdef DEBUG_SETUP
	#define debs(x,y) \
  	cout << x; \
  	element_printf("%B\n", y)
#else
	#define debs(x,y)
#endif

void Setup(pairing_t pairing, element_t* public_key, element_t* master_secret_key) {
  #ifdef DEBUG_SETUP
    cout << "\n-------------------------------- Get into Setup()\n";
  #endif

  element_t temp[3], e_g_h;

  // Generate two instances of the 2-linear assumption
  element_random(master_secret_key[2]);
  debs("\na1 = ", master_secret_key[2]);

  element_random(master_secret_key[3]);
  debs("a2 = ", master_secret_key[3]);

  element_random(master_secret_key[4]);           // b1
  debs("b1 = ", master_secret_key[4]);

  element_random(master_secret_key[5]);           // b2
  debs("b2 = ", master_secret_key[5]);

  for(int i = 0; i < 3; i++) {
    element_init_Zr(temp[i], pairing);
    element_random(temp[i]);   // d1, d2, d3
  }
  debs("d1 = ", temp[0]);
  debs("d2 = ", temp[1]);
  debs("d3 = ", temp[2]);

  // Pick a random element from the two source groups and pair them
  element_random(master_secret_key[0]);
  debs("\ng = ", master_secret_key[0]);

  element_random(master_secret_key[1]);
  element_set(public_key[0], master_secret_key[1]);   // h
  debs("h = ", master_secret_key[1]);

  element_init_GT(e_g_h, pairing);
  element_pairing(e_g_h, master_secret_key[0], master_secret_key[1]);     // e(g,h)
  debs("e(g,h) = ", e_g_h);

  // Now compute various parts of msk and pk
  for (int i = 1; i <= 2; i++)
    element_pow_zn(public_key[i], public_key[0], master_secret_key[1+i]);   // h^a1, h^a2
  debs("\nH1 = h^a1 = ", public_key[1]);
  debs("H2 = h^a2 = ", public_key[2]);

  for (int i = 0; i < 3; i++)
    element_pow_zn(master_secret_key[6+i], master_secret_key[0], temp[i]);    // g^d1, g^d2, g^d3
  debs("\ng^d1 = ", master_secret_key[6]);
  debs("g^d2 = ", master_secret_key[7]);
  debs("g^d3 = ", master_secret_key[8]);

  for (int i = 0; i < 2; i++) {
    element_mul(temp[i], temp[i], master_secret_key[2+i]);      // d1a1, d2a2
    #ifdef DEBUG_SETUP
      element_printf("\nd%da%d = %B\n", i+1, i+1, temp[i]);
    #endif
    element_add(temp[i], temp[i], temp[2]);                     // d1a1 + d3, d2a2 + d3
    #ifdef DEBUG_SETUP
      element_printf("d%da%d + d3 = %B\n", i+1, i+1, temp[i]);
    #endif
    element_pow_zn(public_key[3+i], e_g_h, temp[i]);            // Ti = e(g,h) ^ (diai + d3)
  }
  debs("\nT1 = ", public_key[3]);
  debs("T2 = ", public_key[4]);

  for(int i = 0; i < 3; i++)
    element_clear(temp[i]);
  element_clear(e_g_h);

  #ifdef DEBUG_SETUP
    cout << "\n-------------------------------- Exit Setup()\n";
  #endif
}