# Fast Attribute-based encryption

The pre-existing implementation of FAME (given in [Attribute-based Encryption](https://github.com/sagrawal87/ABE "Click to go to the repository")) is written in Python and uses the CHARM library making it computationally slower.

As proposed for further improvement by the paper [FAME: Fast Attribute-based Message Encryption](https://eprint.iacr.org/2017/807 "Click to view the research paper") by Shashank Agrawal and Melissa Chase, we try to build a comprehensive implementation of the FAME scheme using the **C++ programming language** in this project.

With our work, we are aiming to decrease the time required to set up, generate keys, encrypt and decrypt using C++ to reduce the running time compared to the current Python implementation.

## Usage
The implementation runs as a docker container. The Dockerfile used to create the image is given in the root of the folder structure.

Create the docker image:
```
docker build -t fame:v1 .
```

Create a docker container named `fame`:
```
docker create --name fame -it fame:v1
```

Run the docker container interactively:
```
docker start -i fame
```

Now, run the script inside the docker container that is running interactively:
```
./run.sh
```
You can change the `policy_string` and `attribute list` in the file **sample.cpp** file using vim (available in the docker container) and see how the message gets decrypted if and only if the `attribute_list` satisfies the `policy_string`.

## Troubleshooting
Please report any issues you find so that we can resolve it.
