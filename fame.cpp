// Debugging mode
#define DEBUG
#define DEBUG_SETUP
#define DEBUG_KEYGEN
#define DEBUG_ENCRYPT
#define DEBUG_DECRYPT
#define DEBUG_SUPPORT
#define BENCHMARK 1  // Average time over 'n' calls

using namespace std;

#ifdef BENCHMARK
  #include <chrono>
#endif

#include <iostream>
#include <map>
#include <pbc/pbc.h>
#include <string>
#include <vector>
#include "include/FAME_setup.h"
#include "include/FAME_keygen.h"
#include "include/FAME_encrypt.h"
#include "include/FAME_decrypt.h"

#ifdef DEBUG
	#define deb(x) cout<<x<<endl
#else
	#define deb(x)
#endif

int main() {
  // Attribute list
  vector<string> attribute_list {"ONE", "TWO", "THREE"};
  // Policy to be attached
  string policy_string =  "((ONE and THREE) and (TWO OR FOUR))";

  deb("\n-------------------------------- Starting --------------------------------");

  element_t public_key[5];
  element_t master_secret_key[9];
  pairing_t pairing;

  FILE *fptr = fopen("param/mnt224.param","r");
  if(fptr == NULL)
    exit(1);

  char param[2048];
  size_t count = fread(param, 1, 2048, fptr);
  if(!count)
      pbc_die("input error");
  pairing_init_set_buf(pairing, param, count);

  deb("\n * * * * * * * * * * * * * * * *  SETUP   * * * * * * * * * * * * * * * *");

  // Initializing group for each element in master secret key
  element_init_G1(master_secret_key[0], pairing);
  element_init_G2(master_secret_key[1], pairing);
  for (int i = 0; i < 4; i++)
    element_init_Zr(master_secret_key[2+i], pairing);
  for (int i = 0; i < 3; i++)
    element_init_G1(master_secret_key[6+i], pairing);

  // Initializing group for each element in public key
  for (int i = 0; i < 3; i++)
    element_init_G2(public_key[i], pairing);
  for (int i = 0; i < 2; i++)
    element_init_GT(public_key[3+i], pairing);

  #ifdef BENCHMARK
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;

    high_resolution_clock::time_point time[8][BENCHMARK];

    for(int i = 0; i < (BENCHMARK-1); i++) {
      time[0][i] = high_resolution_clock::now();
      Setup(pairing, public_key, master_secret_key);
      time[1][i] = high_resolution_clock::now();    
    }

    time[0][BENCHMARK-1] = high_resolution_clock::now();
  #endif

  // Run the set up
  Setup(pairing, public_key, master_secret_key);
  #ifdef BENCHMARK
    time[1][BENCHMARK-1] = high_resolution_clock::now();
  #endif

  #ifdef DEBUG
    cout << "\npublic_key =\n";
    element_printf(" h = %B\n", public_key[0]);
    element_printf(" H1 = %B\n", public_key[1]);
    element_printf(" H2 = %B\n", public_key[2]);
    element_printf(" T1 = %B\n", public_key[3]);
    element_printf(" T2 = %B\n", public_key[4]);

    cout << "\nmaster_secret_key =\n";
    element_printf(" g = %B\n", master_secret_key[0]);
    element_printf(" h = %B\n", master_secret_key[1]);
    element_printf(" a1 = %B\n", master_secret_key[2]);
    element_printf(" a2 = %B\n", master_secret_key[3]);
    element_printf(" b1 = %B\n", master_secret_key[4]);
    element_printf(" b2 = %B\n", master_secret_key[5]);
    element_printf(" g^d1 = %B\n", master_secret_key[6]);
    element_printf(" g^d2 = %B\n", master_secret_key[7]);
    element_printf(" g^d3 = %B\n", master_secret_key[8]);
  #endif

  deb("\n * * * * * * * * * * * * * * * *  KEYGEN   * * * * * * * * * * * * * *");

  #ifdef DEBUG
    cout << "\nattribute_list =\n";
    for(auto i:attribute_list)
      cout << "\t" << i << endl;
  #endif

  // Generate a key
  map<string, array<element_t, 3>> sk;
  #ifdef BENCHMARK
    for(int i = 0; i < (BENCHMARK-1); i++) {
      time[2][i] = high_resolution_clock::now();
      Keygen(sk, master_secret_key, attribute_list, pairing);
      time[3][i] = high_resolution_clock::now();

      for(auto i:sk)                          // Clear key
        for(int j=0; j < 3; j++)
          element_clear(i.second[j]);
    }
    time[2][BENCHMARK-1] = high_resolution_clock::now();
  #endif
  Keygen(sk, master_secret_key, attribute_list, pairing);
  #ifdef BENCHMARK
    time[3][BENCHMARK-1] = high_resolution_clock::now();
  #endif

  #ifdef DEBUG
    cout << endl << "sk =\n";
    for(auto i:sk) {
      for(int j=0; j < 3; j++) {
        cout << "sk[" << i.first << "][" << j << "] = ";
        element_printf("%B\n", i.second[j]);
      }
    }
  #endif

  deb("\n * * * * * * * * * * * * * * * *  ENCRYPT   * * * * * * * * * * * * * *");

  // Choose a random message
  element_t message;
  element_init_GT(message, pairing);
  element_random(message);

  BinNode *policy = NULL;

  #ifdef DEBUG
    element_printf("\nmessage = %B\n", message);
    cout << "\npolicy_string = " << policy_string << endl;
  #endif

  // Generate a Ciphertext with policy attached
  map<string, array<element_t, 3>> cipher_text;

  #ifdef BENCHMARK
    for(int i = 0; i < (BENCHMARK-1); i++) {
      time[4][i] = high_resolution_clock::now();
      policy = Encrypt(cipher_text, public_key, policy_string, message, pairing);
      time[5][i] = high_resolution_clock::now();

      for(auto i:cipher_text)                 // Clear cipher text
        for(int j=0; j < 3; j++)
          element_clear(i.second[j]);
      policy->clean();
      delete policy;
    }
    time[4][BENCHMARK-1] = high_resolution_clock::now();
  #endif
  policy = Encrypt(cipher_text, public_key, policy_string, message, pairing);
  #ifdef BENCHMARK
    time[5][BENCHMARK-1] = high_resolution_clock::now();
  #endif

  #ifdef DEBUG
    cout << endl;
    for(auto i:cipher_text) {
      for(int j=0; j < 3; j++) {
        cout << "cipher_text[" << i.first << "][" << j << "] = ";
        element_printf("%B\n", i.second[j]);
      }
    }
  #endif

  deb("\n * * * * * * * * * * * * * * * *  DECRYPT   * * * * * * * * * * * * * *");

  // Decryption using the generate key
  element_t decrypted_message;
  element_init_GT(decrypted_message, pairing);
  #ifdef BENCHMARK
    for(int i = 0; i < (BENCHMARK-1); i++) {
      time[6][i] = high_resolution_clock::now();
      Decrypt(decrypted_message, attribute_list, public_key, policy, cipher_text, sk, pairing);
      time[7][i] = high_resolution_clock::now();
    }
    time[6][BENCHMARK-1] = high_resolution_clock::now();
  #endif
  Decrypt(decrypted_message, attribute_list, public_key, policy, cipher_text, sk, pairing);
  #ifdef BENCHMARK
    time[7][BENCHMARK-1] = high_resolution_clock::now();
  #endif

  #ifdef DEBUG
    element_printf("\ndecrypted_message = %B\n", decrypted_message);
    element_printf("\noriginal_message = %B\n", message);
  #endif

  // Check if decrypted message is correct
  if(!element_cmp(decrypted_message, message))
    cout << "\n Successful decryption.\n";
  else
    cout << "\n Decryption failed.\n";

  for(int i=0; i<5; i++)                  // Clear public key
    element_clear(public_key[i]);
  for(int i=0; i<9; i++)                  // Clear master secret key
    element_clear(master_secret_key[i]);
  for(auto i:cipher_text)                 // Clear cipher text
    for(int j=0; j < 3; j++)
      element_clear(i.second[j]);
  for(auto i:sk)                          // Clear key
    for(int j=0; j < 3; j++)
      element_clear(i.second[j]);
  element_clear(message);
  element_clear(decrypted_message);
  pairing_clear(pairing);
  policy->clean();
  delete policy;

  deb("\n-------------------------------- Ending --------------------------------");
  #ifdef BENCHMARK
    cout << "\n-------------------------------- Timing --------------------------------";
    // Taking average

    chrono::milliseconds dur[4][BENCHMARK+1];
    /* Getting number of milliseconds as an integer. */
    for (int i=0; i < 4; i++) {
      dur[i][BENCHMARK] =  (std::chrono::duration<long int, std::ratio<1, 1000> >)0;
      for (int j=0; j < BENCHMARK; j++) {
        dur[i][j] = duration_cast<milliseconds>(time[1+(2*i)][j] - time[2*i][j]);
        dur[i][BENCHMARK] = dur[i][BENCHMARK] + dur[i][j];      // Total
      }
    }
    auto setup_time = duration_cast<milliseconds>(time[1][0] - time[0][0]);
    auto keygen_time = duration_cast<milliseconds>(time[3][0] - time[2][0]);
    auto encrypt_time = duration_cast<milliseconds>(time[5][0] - time[4][0]);
    auto decrypt_time = duration_cast<milliseconds>(time[7][0] - time[6][0]);

    cout << "\nSetup  :";
    for (int i=0; i < BENCHMARK; i++)
      cout << " " << dur[0][i].count();
    cout << "\nKeygen :";
    for (int i=0; i < BENCHMARK; i++)
      cout << " " << dur[1][i].count();
    cout << "\nEncrypt:";
    for (int i=0; i < BENCHMARK; i++)
      cout << " " << dur[2][i].count();
    cout << "\nDecrypt:";
    for (int i=0; i < BENCHMARK; i++)
      cout << " " << dur[3][i].count();

    cout << "\n\n[Average] Setup   : " << dur[0][BENCHMARK].count() / (float)BENCHMARK << " ms";
    cout << "\n[Average] Keygen  : " << dur[1][BENCHMARK].count() / (float)BENCHMARK << " ms";
    cout << "\n[Average] Encrypt : " << dur[2][BENCHMARK].count() / (float)BENCHMARK << " ms";
    cout << "\n[Average] Decrypt : " << dur[3][BENCHMARK].count() / (float)BENCHMARK << " ms";
    cout << "\n------------------------------------------------------------------------" << endl;
  #endif
  return 0;
}