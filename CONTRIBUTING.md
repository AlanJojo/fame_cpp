# Authors
```
Batch: 2018 - 2022
Course: Bachelor of Technology (B.Tech.)
Institute: National Institute of Technology, Calicut
```

| Roll Number | Name                      |
| ----------: | ------------------------- |
| B180008CS   | Alan Jojo                 |
| B180347CS   | Emmanuel Marian Mathew    |
| B180363CS   | Atar Mohammad Umar Farooq |