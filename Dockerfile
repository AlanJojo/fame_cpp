# Dockerfile used for testing

FROM ubuntu:noble
RUN apt-get update \
  && apt-get install -y g++ make libgmp-dev flex bison libssl-dev wget vim \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Creating user
RUN useradd -ms /bin/bash user

# Download, extract, make and install the PBC files
RUN wget -q https://crypto.stanford.edu/pbc/files/pbc-0.5.14.tar.gz \
  && tar -xzf pbc-0.5.14.tar.gz -C ./ \
  && cd pbc-0.5.14 \
  && ./configure \
  && make -j20 \
  && make install \
  && ldconfig \
  && cd .. \
  && rm -rf pbc-0.5.14/ pbc-0.5.14.tar.gz

# Add FAME files
COPY --chown=user:user . /home/user/

# Set bash as docker entrypoint
USER user
WORKDIR /home/user
ENTRYPOINT /bin/bash