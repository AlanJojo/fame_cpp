#!/bin/bash

clear
printf "Compiling...\n"
g++ -Wall fame.cpp -lpbc -lgmp -lcrypto -o fame
if [[ $? -eq $zero ]]; then     # checks for status of previous command
  printf "\nRunning...\n"
  ./fame
else
  printf "\nCompiling failed...\n"
fi